import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuideComponent } from './event-guide.component';

describe('EventGuideComponent', () => {
  let component: EventGuideComponent;
  let fixture: ComponentFixture<EventGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
