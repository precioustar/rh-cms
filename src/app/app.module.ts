import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { CountdownClockComponent } from './countdown-clock/countdown-clock.component';
import { ServicesSectionComponent } from './services-section/services-section.component';
import { AboutComponent } from './about/about.component';
import { CounterAreaComponent } from './counter-area/counter-area.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { TeamComponent } from './team/team.component';
import { GalleryComponent } from './gallery/gallery.component';
import { FaqComponent } from './faq/faq.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { PricingComponent } from './pricing/pricing.component';
import { EventGuideComponent } from './event-guide/event-guide.component';
import { BlogComponent } from './blog/blog.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { MapComponent } from './map/map.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { CopyrightComponent } from './copyright/copyright.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    CountdownClockComponent,
    ServicesSectionComponent,
    AboutComponent,
    CounterAreaComponent,
    ScheduleComponent,
    TeamComponent,
    GalleryComponent,
    FaqComponent,
    SponsorsComponent,
    PricingComponent,
    EventGuideComponent,
    BlogComponent,
    SubscribeComponent,
    MapComponent,
    ContactComponent,
    FooterComponent,
    CopyrightComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
